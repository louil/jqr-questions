#include <stdio.h>
#include <stdlib.h>

void takesarg(int x){

	printf("X is - %d\n", x);

}

int main(int argc, char *argv[]){

	if (argc != 2){
		printf("Error\n");
		return 0;		
	}

	int x = strtol(argv[1], NULL, 10);

	takesarg(x);

	return 0;
}
