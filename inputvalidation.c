#include <stdio.h>

int main(){

	int a;
	
	while(1){

		printf("Please enter a number: ");
		scanf("%d", &a);

		if (a != 1){
			printf("You entered: %d\n", a);
		}	
		else if (a == 1){
			break;
		}	
	}
	return 0;
}
