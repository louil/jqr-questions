#!/usr/bin/env python3

a = 1
b = 1.1
c = 'h'
d = 'hello'
e = 255
f = 0b101010

print(a)
print(b)
print(c)
print(d)
print(hex(e))
print(f)
