#include <stdio.h>

void valuebyarg(int x, int y);

int main(void){

	int x = 2, y = 3;

	valuebyarg(x, y);

	return 0;
}

void valuebyarg(int x, int y){

	int z = 0;	

	z = x + y;
	printf("Z = %d\n", z);

}
