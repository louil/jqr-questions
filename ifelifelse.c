#include <stdio.h>

int main(void){

	int x = 1, y = 2, z = 3;

	if(x == 2){
		printf("The value of x - %d\n", x);
	}
	else if(x != 1){
		printf("The value of y is - %d and z is - %d\n", y, z);
	}
	else{
		printf("Oh well x, y, and z are - %d %d %d\n", x, y, z);
	}

	return 0;
}
