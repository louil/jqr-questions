#include <stdio.h>

void refer(int *x);

int main(void) {
    int var = 111;
    int *ptr = &var;

    refer(ptr);
    printf("ptr's address %p\n", (void *)&ptr);
    return 0;
}

void refer(int *x) {
    printf("x's address %p\n", (void *)&x);
    x = NULL;
}
