#include <stdio.h>

int main(void){

	int a = 1;
	float b = 1.1;
	char c[] = "hello";
	char d = 'h';
	long int e = 8;

	printf("This is an int %d\n", a);
	printf("This is a float %f\n", b);
	printf("This is a string %s\n", c);
	printf("This is a character %c\n", d);
	printf("This is a long %ld\n", e);

	return 0;
	
}
