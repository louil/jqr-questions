#include <stdio.h>

int main(void){

	int order = 3;

	switch(order){
		case 0:
			printf("This was case 0\n");
		case 1:
			printf("This was case 1\n");
		case 2:
			printf("This was case 2\n");
		case 3:
			printf("Execute order 66\n");
	}

	return 0;
}
