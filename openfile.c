#include <stdio.h>

int main(void){

	int c;

	FILE *fp;

	fp = fopen("Ctester.txt", "r");

	if (!fp){
		return 0;
	}
	else{
		while ((c = getc(fp)) != EOF){
        	putchar(c);
		}
		fclose(fp);
	}

	return 0;
}

