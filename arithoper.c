#include <stdio.h>

int main(void){

	int x = 2;
	int y = 4;
	int add = 0, subtract = 0, divide = 0, modulus = 0;

	add = x + y;
	subtract = y - x;
	divide = y / x;
	modulus = y % x;
	
	printf("Add = %d\nSubtract = %d\nDivide = %d\nModulus = %d\n", add, subtract, divide, modulus);

	return 0;

}
