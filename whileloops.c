#include <stdio.h>

int main(void){

	int x = 1;
	
	while(1){
		if(x < 10){
			x++;
		}
		else if(x == 10){
			printf("X should be 10 - %d\n", x);
			break;
		}
	}

	return 0;
}
