#include <stdio.h>
#include <stdlib.h>

int main(void){

	char **p;
	int i = 0, dim1 = 2, dim2 = 3;

	p = malloc(sizeof(char *) * dim1);

	for (i = 0; i < dim1; i++){
		*(p + i) = malloc(sizeof(char) * dim2);
	}

	for (i = 0; i < dim1; i++){
	  free(p[i]);
	}

	free(p);
	return 0;
}
