#include <stdio.h>
#include <stdlib.h>


int main(void){

	int x = 1;
	int *z;

	z = &x;

	printf("The value of x is - %d\n", x);
	printf("The address of z is - %p\n", (void *)&z);

	return 0;
}
