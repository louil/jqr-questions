#include <stdio.h>

void recursion(int num)
{
	if (num < 0){   
		return;
	}
	else{
		printf("Num is - %d\n", num);   
		recursion(num - 1); 
	}
}

int main(){

	recursion(20); 

}

